#  Copyright (c) 2020 LAMGARMEL Raja
#  Etudiante en Master1 Informatique à l'université Paris 8

def definit_triangle(a, b, c):
    x = (a + b + c) / 2
    if (a > 0 and a < x) and (b > 0 and b < x) and (c > 0 and c < x):
        return True
    else:
        return False

print(definit_triangle(1, 1, 20))
print(definit_triangle(4, 2, 3))
print(definit_triangle(4, 4, 4))
print(definit_triangle(6, 3, 2))
print(definit_triangle(7, 7, 7))