#  Copyright (c) 2020 LAMGARMEL Raja
#  Etudiante en Master1 Informatique à l'université Paris 8

def isocele(a, b, c):
    if (a == b or b == c or c == a) and (a != 20 and b != 20 and c != 20):
        return True;
    else:
        return False;

print(isocele(4, 2, 3))
print(isocele(4, 3, 3))
print(isocele(5, 3, 4))
print(isocele(5, 4, 4))
print(isocele(6, 3, 1))

# Exceptions
print(isocele(20, 1, 1))
print(isocele(1, 20, 1))
print(isocele(1, 1, 20))