#  Copyright (c) 2020 LAMGARMEL Raja
#  Etudiante en Master1 Informatique à l'université Paris 8

from Exercice2 import aire_ordonne
from Exercice3 import definit_triangle

def nb_triangles_speciaux(n, p):
    i = 0
    if (n > 0 and p > 0):
        for a in range(n, p+1):
            for b in range(a+1, p+1):
                for c in range(b+1, p+1):
                    if (definit_triangle(a, b, c) == True) and (aire_ordonne(a, b, c) == a + b + c):
			# print(a,b,c)
                        i = i + 1
    return  i

print(nb_triangles_speciaux(1, 20))
print(nb_triangles_speciaux(5, 15))
print(nb_triangles_speciaux(2, 10))
print(nb_triangles_speciaux(3, 20))
print(nb_triangles_speciaux(6, 50))
print(nb_triangles_speciaux(5, 15))