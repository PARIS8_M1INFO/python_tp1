#  Copyright (c) 2020 LAMGARMEL Raja
#  Etudiante en Master1 Informatique à l'université Paris 8

import math
def aire_ordonne(a, b, c):
    u1 = min(a, b, c)
    u3 = max(a, b, c)

    if (a == u1 and b == u3):
        u2 = c
    elif (a == u1 and c == u3):
        u2 = b
    elif (b == u1 and a == u3):
        u2 = c
    elif (b == u1 and c == u3):
        u2 = a
    elif (c == u1 and b == u3):
        u2 = a
    elif (c == u1 and a == u3):
        u2 = b
    return (1 / 2) * (math.sqrt(((u1 * u1) * (u3 * u3)) - ((u1 * u1 - u2 * u2 + u3 * u3) / 2) * (
            (u1 * u1 - u2 * u2 + u3 * u3) / 2)))


print(aire_ordonne(4, 2, 3))
print(aire_ordonne(4, 3, 3))
print(aire_ordonne(4, 4, 4))
print(aire_ordonne(3, 4, 5))
print(aire_ordonne(13, 14, 15))
print(aire_ordonne(1, 1, 1))
print(aire_ordonne(6, 8, 10))